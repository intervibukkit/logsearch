package ru.intervi.logsearch;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("logsearch.use")) {
			sender.sendMessage("no permission");
			return true;
		}
		if (args == null || args.length == 0) {
			sender.sendMessage("========= LogSearch =========");
			sender.sendMessage("Search: /lsech [args]");
			sender.sendMessage("Arguments:");
			sender.sendMessage("* [regex=] - use regular expression for matching file names");
			sender.sendMessage("* [-noand] - use OR for search parts in logs");
			sender.sendMessage("* [-nocase] - no case sensitivity");
			sender.sendMessage("Example: /lsech CChannels InterVi");
			sender.sendMessage("Example regex: /lsech regex=^([2][0][1][8]-[0][2]-[0-2][0-9]-[\\d]*)(\\.log\\.gz)$ InterVi");
			sender.sendMessage("Example 2: /lsech -noand -nocase InterVi Afellia");
			return true;
		}
		String regex = null;
		boolean and = true, nocase = false;
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (arg.length() == 6) {
				arg = arg.substring(0, 6);
				if (arg.equalsIgnoreCase("-noand")) {
					and = false;
					args[i] = null;
				}
			} else if (arg.length() == 7) {
				arg = arg.substring(0, 7);
				if (arg.equalsIgnoreCase("-nocase")) {
					nocase = true;
					args[i] = null;
				}
			} else if (arg.length() > 7) {
				if (arg.substring(0, 6).equalsIgnoreCase("regex=")) {
					regex = arg.substring(6).trim();
					args[i] = null;
				}
			}
		}
		new Search(args, sender, regex, and, nocase).start();
		sender.sendMessage("Search starting, please wait...");
		return true;
	}
}
