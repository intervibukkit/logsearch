package ru.intervi.logsearch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

import org.bukkit.command.CommandSender;

public class Search extends Thread {
	private final String[] ARGS;
	private final CommandSender SENDER;
	private final File PATH;
	private final String REGEX;
	private final boolean AND, NOCASE;
	
	protected Search(String[] args, CommandSender sender, String regex, boolean and, boolean nocase) {
		ARGS = args;
		SENDER = sender;
		PATH = new File(new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()
				).getParentFile().getParentFile(), "logs");
		REGEX = regex;
		AND = and;
		NOCASE = nocase;
	}
	
	private File getLogFile() {
		File logs = new File(this.PATH, "logsearch");
		if (!logs.isDirectory()) logs.mkdirs();
		File file = new File(logs, "logsearch.log");
		int i = 1;
		while (file.isFile()) {
			file = new File(logs, "logsearch" + String.valueOf(i) + ".log");
			i++;
		}
		return file;
	}
	
	public static boolean isLog(String name) {
		int dpos = name.lastIndexOf('.');
		if (dpos == -1) return false;
		if (name.substring(dpos).equalsIgnoreCase(".log")) return true;
		return false;
	}
	
	public static boolean isGZ(String name) {
		int dpos = name.lastIndexOf('.');
		if (dpos == -1) return false;
		if (name.substring(dpos).equalsIgnoreCase(".gz")) return true;
		return false;
	}
	
	public static ArrayList<String> search(InputStream input, String[] args, boolean and, boolean nocase) throws IOException {
		ArrayList<String> list = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		w: while (reader.ready()) {
			String line = reader.readLine();
			for (int i = 0; i < args.length; i++) {
				String arg = args[i];
				if (arg == null) continue;
				if (and) {
					if (((!nocase && line.indexOf(arg) == -1) || (nocase && line.toLowerCase().indexOf(arg.toLowerCase()) == -1))
							&& !line.matches(arg)) {
						continue w;
					}
				} else {
					if (((!nocase && line.indexOf(arg) != -1) || (nocase && line.toLowerCase().indexOf(arg.toLowerCase()) != -1))
							|| line.matches(arg)) {
						list.add(line);
						continue w;
					} else if (i + 1 == args.length) continue w;
				}
			}
			list.add(line);
		}
		reader.close();
		return list;
	}
	
	@Override
	public void run() {
		BufferedWriter writer = null;
		try {
			System.out.println("Log path: " + PATH.getAbsolutePath());
			File file = this.getLogFile();
			writer = new BufferedWriter(new FileWriter(file));
			for (File log : this.PATH.listFiles()) {
				try {
					if (!log.isFile()) continue;
					if (REGEX != null && !log.getName().matches(REGEX)) continue;
					ArrayList<String> list = new ArrayList<String>();
					if (Search.isLog(log.getName())) list = Search.search(new FileInputStream(log), ARGS, AND, NOCASE);
					if (Search.isGZ(log.getName()))
						list.addAll(Search.search(new GZIPInputStream(new FileInputStream(log)), ARGS, AND, NOCASE));
					if (list.isEmpty()) continue;
					writer.write("================================ " + log.getName() + " ================================");
					writer.newLine();
					for (String line : list) {
						writer.write(line);
						writer.newLine();
					}
					writer.newLine();
				}
				catch(EOFException e) {}
				catch(Exception e) {e.printStackTrace();}
			}
			if (this.SENDER != null) this.SENDER.sendMessage("Search done! Log: " + file.getAbsolutePath());
		}
		catch(IOException e) {e.printStackTrace();}
		catch(Exception e) {e.printStackTrace();}
		finally {
			try {
				if (writer != null) writer.close();
			} catch(Exception e) {e.printStackTrace();}
		}
	}
}
